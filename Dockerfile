ARG JETPACK_VERSION="r32.5.0"
#defining the version of the base image we want to use

FROM registry.hub.docker.com/mdegans/l4t-base:${JETPACK_VERSION}
#geting the baseimage

RUN mkdir /coronapas-scanner/
COPY coronapas-scanner/ /coronapas-scanner/
# copying all the necesary files and folders to the Dockerfile

RUN apt-get update &&\
    apt-get upgrade -y
RUN apt-get install -y python3-pip
RUN python3 -m pip install --upgrade pip
RUN apt-get install nano

RUN echo 'deb http://private-repo-1.hortonworks.com/HDP/ubuntu14/2.x/updates/2.4.2.0 HDP main' >> /etc/apt/sources.list.d/HDP.list
RUN echo 'deb http://private-repo-1.hortonworks.com/HDP-UTILS-1.1.0.20/repos/ubuntu14 HDP-UTILS main'  >> /etc/apt/sources.list.d/HDP.list
RUN echo 'deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/azurecore/ trusty main' >> /etc/apt/sources.list.d/azure-public-trusty.list
RUN echo 'deb https://repo.download.nvidia.com/jetson/common 10 r32.4 main' > /etc/apt/sources.list.d/nvidia-l4t-apt-source.list
RUN echo 'deb https://repo.download.nvidia.com/jetson/t194 3 r32.4 main' >> /etc/apt/sources.list.d/nvidia-l4t-apt-source.list
# later on we run alot of things that needs to know ware to look for the version we want and that is what we are telling it here

RUN apt-get install -y build-essential cmake libavcodec-dev libavformat-dev libavutil-dev libeigen3-dev libglew-dev libgtk2.0-dev libgtk-3-dev libjpeg-dev libpng-dev libpostproc-dev libswscale-dev libtbb-dev libtiff5-dev libv4l-dev libxvidcore-dev libx264-dev qt5-default zlib1g-dev pkg-config
RUN apt-get install -y git software-properties-common sed curl apt-utils cmake libatlas-base-dev gfortran libhdf5-serial-dev hdf5-tools python3-dev locate libfreetype6-dev python3-setuptools protobuf-compiler libprotobuf-dev  openssl libssl-dev libcurl4-openssl-dev cython3 libxml2-dev libxslt1-dev build-essential pkg-config libtbb2 libtbb-dev libavcodec-dev libavformat-dev libswscale-dev libxvidcore-dev libavresample-dev libtiff-dev libjpeg-dev libpng-dev libgtk-3-dev libcanberra-gtk-module libcanberra-gtk3-module libv4l-dev libdc1394-22-dev virtualenv zbar-tools network-manager
#installing a lot of things to optemize python and more

WORKDIR /coronapas-scanner/
COPY setup.py .
RUN pip3 install -e .
COPY requirements.txt .
RUN pip3 install -r requirements.txt  
#runniging the rest of the requirements from requirements.txt

RUN pip3 uninstall -y cryptography 
RUN pip3 install cryptography 
#for some reason cryptography dosent work, and be unistalling and then installing it agian makes it work

RUN pip3 install cmake

WORKDIR /coronapas-scanner/coronapas_scanner/src/
RUN git clone https://github.com/JetsonHacksNano/buildOpenCV.git
WORKDIR /coronapas-scanner/coronapas_scanner/src/buildOpenCV/
RUN sed -i 's/sudo//' buildOpenCV.sh
RUN sed -i 's/WITH_CUDA=ON/WITH_CUDA=OFF/' buildOpenCV.sh
RUN sed -i 's/CUDA_FAST_MATH=ON/CUDA_FAST_MATH=OFF/' buildOpenCV.sh
#editing the buildOpenCV to make it run in a docker container

RUN ./buildOpenCV.sh
#this is making sure we get opencv witch is for som reasone not workking without it

WORKDIR /coronapas-scanner/coronapas_scanner/src/
COPY run.sh .
RUN chmod a+x run.sh
CMD ["./run.sh"]
#making sure that when we open the the container that we open it in the right folder
#CMD ["python3", "app_server.py", "&&", "main.py"]

# after runing docker build use the next line to run it
# sudo docker run -it -e LANG=C.UTF-8 --net=host --privileged --runtime nvidia -v /tmp/argus_socket:/tmp/argus_socket --volume /var/run/dbus:/var/run/dbus [image name]