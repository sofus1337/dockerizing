#!/bin/bash

set -e

exec python3 app_server.py &
exec python3 main.py