Developer Documentation
Developer documentation is product documentation that explains how your software should work, so developers know what they’re building. These documents include build requirements that contain what the software is supposed to do, architecture information that explains the components and functions of the software, and checklists that guide developers through the process of building the software.

End-User Documentation
Software end-user documentation is a form of process documentation that provides instructions on using, installing, and configuring your software. Having this kind of documentation available makes it easier for people to understand how to use your software. End-user documentation includes user guides, knowledge bases, tutorials, troubleshooting manuals, reference guides, and release notes.


making a docker container 
if you need a docker container you first need a docker image to make it from, you can ether pick one from dockerhub that fits what you want to do with the container,
or you can make your own with a dockerfile.
if you pick an image from docker hub you first need to pull the image by: docker pull . -t #IMAGENAME

then you can run the image and do what you want to do with it as a self containt container by: `docker run -it #IMAGENAME` (there are more tags then i and t that you can use
but if it just a simple container and do not need a lot from it -it is all you need)

if you have a container that is already running you can get in to it by: `docker exec -it #CONTAINERNAME bash` (depening on the image it might not be bash
it might be sh or somting else)

To see the running containers use `docker ps`, and you get a list over all containers, all infomation about the container will also be listet like
when it was made what image it is running on, what cammand it uses(if you will be using bash or somthing else) and its name. To see all the containers not just running once add the -a tag: `docker ps -a`




if you want to have your own image you will first have to make a dockerfile you do that by fist makeing a dockerfile and call it Dockerfile 
with cabetil d else the docker system dont know what file to use when making the image

then you will pick baseimage to build your image on, know you dont need this but in most cases you should pick one, pyhton for instens have have 
a lot of things already instalt so you dont have to in your docker file, so if you have a aplication you want to run in a docker container 
it migth be an idér to look at what languse it is readen in and see if there an image for that languhe, if there is use that for your basse image
you pick the basseimage whit the FROM tag and specify the specifig version to rigth the version you want using ":version", if dont pick a specifig 
the docker will just get the latest version from docker hub, so picking a baseimage looks like: "FROM BASEIMAGE:VERSION"

If there are somthing you need to ad to the docker image you can ad it by using the RUN tag, with this tag you can use all the comands that you
would in a normel linux, tho you do not need to use sudo sicne you are already in root unles you have spesefired ealyer withe the USER tag that you are not root.
use the RUN tag like this: "RUN apt-get install -y something somthing"

if you need to change directory and go to a spesifick folder use WORKDIR insted of cd, you do this by:"WORKDIR /your/fav/folder/"

if you need a file in your docker image that are locatet on the computer ware you making the docker image you can use the COPT or ADD tags,
usely the COPY tag is the one you wanna use since ADD has some more things it dose some times it migth do more then what you want it to do.
the tags can both copy files and folders to the docker image. you can also specify ware you want the copyd folder/file is con be in the image
by using it like: `COPY /big/pee/pee/ /my/pants/`

the last line in you dockerfile is properly CMD, CMD is used to run a the aplication you want to run when you starts your container.
you can have more then one CMD tags in the dockerfile but only the last CMD tag will be used so it would be best to but it last. the stuff you
would like to run need to be inside [], and evrey word need to its oven string and put in "" and be seberaded with a coma,
so it will look like this: `CMD ["your", "aplication"]`




when you have made a Dockerfile you need to build the docker image, and you do that by runging: "docker build . -t #IMAGENAME" in the terminal
in the directori with the Dockerfile, -t is a tag used to name the docker file, if it is not used docker will give the image itself.



