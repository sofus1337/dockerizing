# To install the package run 'pip3 install -e .'
from setuptools import setup

setup(
    name="coronapas_scanner",
    version="2.0.0",
    packages=['coronapas_scanner'],
    package_dir={'coronapas_scanner': 'coronapas_scanner/src/'},
    author="Really A Robot",
    author_email="info@reallyarobot.com",
    description="COVID-19 EU Digital Certificate scanner",
    licence='',
    url="https://bitbucket.org/eugene_galaxy/coronapas-scanner",
    python_requires='>=3.6',
)
