# Making and using a Dockerfile 


#### This manual will try to explain how to set up the docker image and run a container with the image 

1. Open a terminal 
2. Run `cd Dockerizing` (cd into the folder with the Dockerfile)
3. Run `sudo docker build . -t #IMAGENAME` (this will build the server image that will run in the container)
4. **Be patient** (it might take som what more than a hour)
5. then you make a container based on the docker image you just made`sudo docker run -it -e LANG=C.UTF-8 --net=host --privileged --runtime nvidia -v /tmp/argus_socket:/tmp/argus_socket --volume /var/run/dbus:/var/run/dbus [image name]`
#
# 
#### Making a docker container 
If you need a docker container you first need a docker image to make it from, you can ether pick one from dockerhub that fits what you want to do with the container,
or you can make your own with a dockerfile.
If you pick an image from docker hub you first need to pull the image by: `docker pull . -t #IMAGENAME`

Then you can run the image and do what you want to do with it as a self-contained container by: `docker run -it #IMAGENAME` (there are more tags than i and t that you can use
but if it just a simple container and do not need a lot from it -it is all you need)

If you have a container that is already running you can get in to it by: `docker exec -it #CONTAINERNAME bash` (depending on the image it might not be bash 
it might be sh or something else)

To see the running containers use `docker ps`, and you get a list over all containers, all information about the container will also be listed like
when it was made what image it is running on, what command it uses(if you will be using bash or something else) and its name. To see all the containers not just running once add the -a tag: `docker ps -a`
##
#### Making a docker image
If you want to have your own image you will first have to make a dockerfile you do that by fist making a dockerfile and call it Dockerfile 
with capital d else the docker system don't know what file to use when making the image.

First you will pick base image to build your image on, know you don't need this but in most cases you should pick one, python for e.g. have 
a lot of things already installed, so you don't have to in your docker file, so if you have a application you want to run in a docker container 
it might be an idér to look at what language it is written in and see if there is an image for that language, if there is use that for your base image
you pick the base image with the FROM tag and specify the specific version to right the version you want using ":version", if you don't pick a specific 
the docker will just get the latest version from docker hub, so picking a base image looks like: "FROM BASEIMAGE:VERSION" `FROM python:3.6`

If you want to define a variables in your dockerfile, you need to use ARG command, so you can call on it latter, you use it like :`ARG BIG_PEEPEE="3.6"`
									      `FROM python:{$BIG_PEEPEE}`

If there are something you need to add to the docker image you can add it by using the RUN tag, with this tag you can use all the commands that you 
would in a normal linux, tho you do not need to use sudo since you are already in root unless you have specified earlier with the USER tag that you are not root.
use the RUN tag like this: `RUN apt-get install -y something somthing`

If you need to change directory and go to a specific folder use WORKDIR instead of cd, you do this by: `WORKDIR /your/fav/folder/`

If you need a file in your docker image that are located on the computer ware you're making the docker image you can use the COPY or ADD tags,
usually the COPY tag is the one you want to use since ADD has some more things it does, sometimes it might do more than what you want it to do.
The tags can both copy files and folders to the docker image. You can also specify ware you want the copied folder/file is con be in the image
by using it like: `COPY /big/pee/pee/ /my/pants/`

The last line in you dockerfile is properly CMD, CMD is used to run the application you want when you start your container.
You can have more then one CMD tags in the dockerfile but only the last CMD tag will be used, so it would be best to, but it last. the stuff you
would like to run need to be inside [], and every word need to its oven string and put in "" and be seperated with a coma,
so it will look like this: `CMD ["your", "aplication"]`


Dockerfile example: 
``` dockerfile
FROM python:lates
RUN apt-get update &&\
    upgrade
WORKDIR /your/work/folder/
COPY /main/script/folder .
CMD ["python", "main.py"]
```




When you have made a Dockerfile you need to build the docker image, and you do that by running: `docker build . -t #IMAGENAME` in the terminal
in the directory with the Dockerfile, -t is a tag used to name the docker file, if it is not used docker will give the image itself.
###
When you have made the image, you run it to make a container, an isolated environment that virtualizes the os to run the application you have, 
you can name the container with the name tag    --name=#CONTAINERNAME.To start a container that you have on the devise: `sudo docker start -i #CONTAINERNAME`	the tag -i is used to make the container interactive.
you don’t need it if the container just have runs your code, and you don’t write everything in the command line. To shut down the container when it isn’t needed open a terminal and write:
`sudo docker stop #CONTAINERNAME`	 or in the container type `exit` (typing exit doesn't guarantee the container to stop, and it will sometimes still continue to run in the background) 
to see all containers running: `sudo docker ps`     to see all containers on the devise end it with the tag -a   `sudo docker ps -a`


Useful Docker links:  
https://linuxhint.com/create-dockerfile/

https://codefresh.io/Docker-Tutorial/build-docker-image-dockerfiles/

https://docs.docker.com/desktop/windows/install/

#### What to do now
A possible place to go now is balenaOS. BalenaOS is an os made for embedded devices and maneging containers. 
It only contains the essentials for running docker reliably on an embedded device.
It is build to last and survive on harsh networks and unexpected shutdowns 

#### Alternatives to Docker
* 1 Podman: doesn't run in root, so it might be more secure. Another reason you might consider Podman is its integration with other developer tools. Podman enjoys wider integration with developer tools than Docker, which offers you a compatible API that makes switching possible.


