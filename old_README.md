Coronapas-scanner installation process for Jetson Nano

This manual will explain step-by-step how to install a working copy of coronapas-scanner to a Jetson Nano running on a microSD. 


Step 1: Flashing the OS to an SD card

Requirements:
    • Clean microSD 64GB (minimum), good R/W speed, e.g. 160MB/s – 60MB/s
    • Jetson Nano (we use model called “Jetson Nano Developer Kit”)
    • Host computer with either SD card reader or USB-to-MicroSD adapter (laptop or PC, doesn’t matter. A device where you will download OS image and flash it to the microSD)
Process:
    • Follow step-by-step tutorial written by Nvidia: https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit
Step 2: Getting started
    • Insert ethernet cable, keyboard, a monitor via HDMI and a power cable to the Jetson Nano
    • Logging into Jetson the first time:
1. Accept the terms of these licences
2. Choose English as language
3. Pick keyboard (I typically take Danish as I have a Danish keyboard)
4. Select Copenhagen as where you are
5. Select username (cpxxxx where xxxx is the number such as 0002 for number two)
6. Select the same name for computer
7. Same as username
8. Password: 123
9. Select “log in automatically”
10. Select APP partition Size as the maximum accepted size
11. Create a SWAP File
12. Set Nvpmodel mode to MAXN
13. Wait for everything to install - the jetson might restart during the installation

Step 3: Downloading Coronapas-scanner Repo & Prepare system for installing Coronapas-scanner libraries

Requirements:
    • Internet connection 
    • Username and password for Bitbucket
Process:
    1. Go to Terminal and run: git clone https://USERNAME@bitbucket.org/eugene_galaxy/coronapas-scanner.git
        1.1 Where USERNAME is your unique username which has been accepted by Eugene
        1.2 Enter your password and get the repo - advice is to now open up the README file in /coronapas-scanner folder to copy/paste into terminal (ctrl+c and ctrl+alt+v for pasting in terminal) This will take approx 4 minutes and a configurator from docker.io need to be accepted
    2. Update/upgrade your system with: sudo apt-get update && sudo apt-get upgrade – This might take a while and you might have to press (y) and accept the download and possibly some docker update.
    3. Open Software & Updates GUI app and tick-in (enable) “Source code” in first tab, and “Canonical Partners” & “Canonical partners (Source code)” fields in “Other Software”. This can be found under “preferences”
    4. Install pip3 with: sudo apt-get install python3-pip
    5. Update pip3 with: python3 -m pip install --upgrade pip
    6. Install nano with: sudo apt-get install nano
    7. Installing some random stuff that might help to install python packages:
       sudo apt-get install curl apt-utils git cmake libatlas-base-dev gfortran libhdf5-serial-dev hdf5-tools python3-dev locate libfreetype6-dev python3-setuptools protobuf-compiler libprotobuf-dev openssl libssl-dev libcurl4-openssl-dev cython3 libxml2-dev libxslt1-dev build-essential pkg-config libtbb2 libtbb-dev libavcodec-dev libavformat-dev libswscale-dev libxvidcore-dev libavresample-dev libtiff-dev libjpeg-dev libpng-dev python-tk libgtk-3-dev libcanberra-gtk-module libcanberra-gtk3-module libv4l-dev libdc1394-22-dev virtualenv
       

Step 4: Installing Coronapas-scanner requirements and packages

    1. Terminal → cd coronapas-scanner/ → pip3 install -e .   (This will install python package of #NoFever)
    2. Run: sudo pip3 install -r requirements.txt  (need to check if we can remove some libs, can not find pyrealsense)
    3. sudo apt-get install zbar-tools
    4. Install pyrealsense2: tutorial at https://github.com/IntelRealSense/librealsense/issues/6964
        1. Go to https://github.com/IntelRealSense/librealsense/releases
        1. Download source_code.zip & unpack it somewhere (it goes to Download folder)
        2. cd /Download/librealsense..... and mkdir build && cd build
        3. Run cmake ../ -DFORCE_RSUSB_BACKEND=ON -DBUILD_PYTHON_BINDINGS:bool=true -DPYTHON_EXECUTABLE=/usr/bin/python3
        4. Run sudo make -j1 && sudo make install
            4.1 This will take approx 1 hour 20 mins where it will pont you for a password to make install
        5. cd wrappers/python (inside build directory)
        6. Copy these 3 files into coronapas-scanner/coronapas_scanner/src
            ▪ pyrealsense2.cpython-36m-aarch64-linux-gnu.so
            ▪ pyrealsense2.cpython-36m-aarch64-linux-gnu.so.2.xx
            ▪ pyrealsense2.cpython-36m-aarch64-linux-gnu.so.2.xx.x
For some reason I had to uninstall / install cryptography for it to work, will confirm if this is needed soon:
sudo pip3 uninstall cryptography
sudo pip3 install cryptography

Step 5: Hotspot, boot scripts, USB rules…


    1. Copy USB udev-rules to allow jetson users to use Realsense:
           cd ~/coronapas-scanner/coronapas_scanner/misc && sudo cp 99-realsense-libusb.rules /etc/udev/rules.d/
           After run: sudo udevadm control --reload-rules && udevadm trigger
    2. Set Label Printer auto-disable to off: 
        1. a. Give admin rights to usb port where printer is in:	
           sudo chown root:lpadmin /dev/usb/lp0
        2. Send a message to the printer address to completely disable auto turn-off:
           sudo echo -n -e '\x1b\x69\x55\x41\x00\x00' > /dev/usb/lp0
        3. (DOESNT WORK, DONT KNOW WHY) As extra, one can set printer to be enabled everytime it is plugged to power!
           sudo echo -n -e '\x1b\x69\x55\x70\x00\x01' > /dev/usb/lp0
    3. Create a WiFi Hotstop: Step 8 in this tutorial: https://mushr.io/hardware/nano_install/
       (Connections → Edit Connections → Create New → Choose a Connection Type: WiFi →put wifi name/ssid. In Security tab, set password)
       Hugo's tutorial: 
        1. Rightclick on the ethernet/wifi icon in the lower right corner of the screen
        2. Press "Edit connections"
        3. Press the "plus" sign and select WI-FI
        4. Fill in the fileds under the tab Wi-Fi
            4.1 Connection name USERNAME (for example cp0002)
            4.2 SSID: Same (cp0002)
            4.3 under Mode select "Hotspot"
            4.4 udner Device select wlan0 (00:xx:xx:xx:xx) - make sure wifi dongle or internet board is pluged in
        5. Go to the "Wi-Fi Security" tab
            5.1 select WPA & WPA2 Personal
            5.2 enter password: jetson123    (show password to make sure and confirm it is jetson123)
            5.3 press save
        6. Make sure that if you had previously connected to a wifi, to forget it
    4. Create & enable auto booting rc.local script:
       go to ~/coronapas-scanner/coronapas_scanner/misc/ and sudo nano rc.local - change the "cp0003" to the current username
       sudo cp ~/coronapas-scanner/coronapas_scanner/misc/rc.local /etc/rc.local                                                                                                                                                                                                                                                                                                                                                                                                                                                      sudo chmod +x /etc/rc.local
       sudo chmod +x  ~/coronapas-scanner/coronapas_scanner/config/boot.sh
       sudo systemctl enable rc-local
       sudo chown root /etc/rc.local
       sudo chmod 755 /etc/rc.local
       
Auto boot enabling:
sudo visudo
#under %sudo ALL..... add
USERNAME ALL=(ALL) NOPASSWD: ALL #Should change to only give permition to selected file in the future such that USERNAME ALL=(ALL) NOPASSWD: home/USERNAME/coronapas-scanner/coronapas_scanner/config/boot.sh
#USERNAME should be replaced by the username of the device

Optimizing speed:
Fan configuration:
cd
git clone https://github.com/Pyrestone/jetson-fan-ctl
cd jetson-fan-ctl
./install.sh

Dissable GUI:
sudo systemctl set-default multi-user.target
sudo reboot # (be patient this might take 10 mins ish)

If you need to enable GUI again:
sudo systemctl set-default graphical.target
sudo reboot



Add shortcuts to launch app_server.py and main.py:
echo >> ~/.bashrc "alias cp_server='python3 /home/$USER/Documents/projects/coronapas-scanner/coronapas_scanner/src/app_server.py'"
echo >> ~/.bashrc "alias cp_main='python3 /home/$USER/Documents/projects/coronapas-scanner/coronapas_scanner/src/main.py'"