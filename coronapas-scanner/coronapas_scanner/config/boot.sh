#!/bin/sh
clear
python3 ~/coronapas-scanner/coronapas_scanner/src/app_server.py &
sleep 10
python3 ~/coronapas-scanner/coronapas_scanner/src/main.py &
sleep 60
python3 ~/coronapas-scanner/coronapas_scanner/src/pid_check.py &

## How to add this bash script to the system's autoboot:
## 1. sudo nano /etc/rc.local
## 2. Add this two lines before last line 'exit 0':
## sleep 30
## sudo -H -u jetson sudo /home/jetson/coronapas_scanner/coronapas_scanner/config/boot.sh


## Save & close the file.
## This will wait 30 seconds after the system boots up, then will run boot.sh (this file.)