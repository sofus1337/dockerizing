This manual will explain how to setup a new label printer.

Some details:
-------------------------
Printer vendor: Brother
Model: QL-700
Label size: 62 endless
Label pixel size - 694px in length, 300px (we use, but not min/max) in width  <- This info is needed when one creates a label image.
Backend that we use: pyusb
-------------------------

System Instructions:
-------------------------
1. By default, printer has 60 minutes auto turn-off. To disable that, two steps are needed:
    a. Give admin rights to usb port where printer is in:
        sudo chown root:lpadmin /dev/usb/lp0
    b. Send a message to the printer address to completely disable auto turn-off:
        sudo echo -n -e '\x1b\x69\x55\x41\x00\x00' > /dev/usb/lp0
    c. (DOESNT WORK, DONT KNOW WHY) As extra, one can set printer to be enabled everytime it is plugged to power!
        sudo echo -n -e '\x1b\x69\x55\x70\x00\x01' > /dev/usb/lp0

    More settings here: https://github.com/pklaus/brother_ql/issues/50

2. Need to install python module to use the printer:
    pip3 install brother_ql

    Package on pypi: https://pypi.org/project/brother-ql/

3. If printer is indended in the particular nofever, make sure the variable that enables it is set to 'True' in the 'nofever_settings.ini' file!!!
    Go to nofever/config/nofever_settings.ini -> find 'LABEL_PRINTER_ENABLED = True'. SET IT TO TRUE if it is False!
-------------------------

Behavior:
-------------------------
The code is written in a way that:
1. The code will initialize regardless if the printer is connected to the system or not.
2. When a user gets scanned and result screen is shown, the code will print a label if:
    a. Printer is connected with USB cable to Jetson
    b. Printer is turned on (green LEd on the top cover)
    c. Paper Roll is mounted correctly inside the bracket
    d. Paper Roll is passed to the feeder (the exit or "mouth" of the printer)
    e. The lid enclosing the roll is CLOSED.
3. If any of the above mentioned conditions is not met -> the code will ignore the printer and work as usual.
4. On the right side of the printed ticket, there can be an image (e.g. QR-code). Here is how the code does this:
    a. the code looks for ALPHABETICALLY named first file, taking ONLY jpg/jpeg/png format, inside nofever/labels/ directory.
    b. if the image exceeds width-lenght size in pixels, stated in nofever_settings.ini, the image will be resized.
    c. if the directory nofever/labels/ is empty, or contains no readable file in jpg/jpeg/png format -> right side of the ticket will be empty.
--------------------------