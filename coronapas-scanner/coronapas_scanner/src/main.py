
import sys
import os
import time
import traceback
import datetime
import warnings

import requests

from utils import CONFIG_SETTINGS, DebugPrint, INT
from logger import LOG
from algorithm import CoronapasScanner

# To disable RuntimeWarning popped by numpy (harmless and accounted for)
warnings.filterwarnings("ignore")

# Initializing debug prints as 'DEBUG' object
DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)

# Initializing config settings as "SETTINGS" object
SETTINGS = CONFIG_SETTINGS['main']

FORCE_INTERNET = SETTINGS.getboolean('FORCE_INTERNET')
NIGTHLY_REBOOT_ENABLED = SETTINGS.getboolean('NIGHTLY_REBOOT_ENABLED')
DEBUG_MEMORY_TRACKER_ENABLED = SETTINGS.getboolean('DEBUG_MEMORY_TRACKER_ENABLED')

if NIGTHLY_REBOOT_ENABLED is True or DEBUG_MEMORY_TRACKER_ENABLED is True:
    from apscheduler.schedulers.background import BackgroundScheduler
if DEBUG_MEMORY_TRACKER_ENABLED is True:
    import psutil


if NIGTHLY_REBOOT_ENABLED is True:
    def reboot_with_screen():
        DEBUG('Initiating Nightly Reboot.')
        requests.get(HOST + 'reboot')
        os.system('sudo reboot')

    scheduler = BackgroundScheduler()
    scheduler.add_job(reboot_with_screen, 'cron', month=SETTINGS['MONTH'],
                      day_of_week=SETTINGS['WEEKDAY'], hour=SETTINGS['HOUR'], minute=SETTINGS['MINUTE'])

if DEBUG_MEMORY_TRACKER_ENABLED is True:
    def RAM_usage_in_megabytes():
        usage = MEMORY_TRACKER.memory_full_info().rss / 1024 ** 2
        DEBUG('Process uses {:1.2f} MB.'.format(usage))
        LOG.critical('Process uses {:1.2f} MB.'.format(usage))

    scheduler = BackgroundScheduler()
    MEMORY_TRACKER = psutil.Process()
    scheduler.add_job(RAM_usage_in_megabytes, 'cron', minute='0,5,10,15,20,25,30,35,40,45,50,55')
    # scheduler.add_job(RAM_usage_in_megabytes, 'cron', minute='0-59')
    scheduler.start()

# Absolute path to this file. Used to get relative paths to YOLO, logger, and PID saver.
base_path = os.path.dirname(os.path.abspath(__file__))

# Providing path for PID.txt
REL_PID_PATH = SETTINGS['PID_PATH']
PID_PATH = os.path.join(os.path.dirname(base_path), REL_PID_PATH)

# App server connection info
IP_ADDRESS = SETTINGS['IP_ADDRESS']
PORT = SETTINGS['PORT']
HOST = 'http://{0}:{1}/'.format(IP_ADDRESS, PORT)


# Has to be here and not in 'pid_check.py' to correctly import LOG from logger.py
# Otherwise, if two separate processes (main.py and pid_check.py) run, LOGS are dublicated in the text file.
def save_pid(pid_path):
    """Gets Process ID of this file and writes it a file provided in the 'pid_path'.

    Args:
        pid_path (string): Full absolute path to to a text file (Doesnt need to exist prior to use).
    """
    pid = os.getpid()
    DEBUG('PID is {}'.format(pid))
    LOG.warning('PID is {}'.format(pid))
    f = open(pid_path, "w+")
    f.write(str(pid))
    f.close()


if __name__ == '__main__':
    try:
        DEBUG('\n==========================  STARTING NEW SESSION  ==========================')
        LOG.warning('\n==========================  STARTING NEW SESSION  ==========================')
        system_error = False
        save_pid(PID_PATH)
        DEBUG('Connecting to 4G Dongle...')
        LOG.warning('Connecting to 4G Dongle...')
        internet_connected = INT.connect(force_internet=FORCE_INTERNET)
        time.sleep(2)  # Delay between connecting 4G Dongle hw and requesting data from internet.
        if internet_connected is False:
            requests.get(HOST + 'reboot')
            DEBUG('No connection to 4G Dongle after forced attempts. Rebooting system.')
            LOG.warning('No connection to 4G Dongle after forced attempts. Rebooting system.')
            system_error = True
            raise SystemError('Program terminated. Awaiting PID checker to catch it and start a reboot.')

        if internet_connected is True:
            DEBUG('Internet - Connected. (FORCE_INTERNET = {})'.format(FORCE_INTERNET))
            LOG.warning('Internet - Connected (FORCE_INTERNET = {})'.format(FORCE_INTERNET))

        DEBUG('Requesting the time from the NTPClient.')
        LOG.warning('Requesting the time from the NTPClient.')
        status = INT.update_time(force_internet=SETTINGS.getboolean('FORCE_INTERNET'))
        if status is False:
            requests.get(HOST + 'reboot')
            DEBUG('No internet connection after forced attempts (time updating). Rebooting system.')
            LOG.warning('No internet connection after forced attempts (time updating). Rebooting system.')
            system_error = True
            raise SystemError('Program terminated. Awaiting PID checker to catch it and start a reboot.')

        DEBUG('Current date & time: {}'.format(str(datetime.datetime.now())))
        LOG.warning('Current date & time: {}'.format(str(datetime.datetime.now())))
        INT.disconnect()
        CoronapasScanner(HOST)
    except Exception:
        if system_error is False:
            requests.get(HOST + 'reboot')
        exc_type = sys.exc_info()[0]
        DEBUG(traceback.format_exc())
        DEBUG('Shutting down...')
        LOG.critical(traceback.format_exc())
        LOG.critical('Shutting down...')
