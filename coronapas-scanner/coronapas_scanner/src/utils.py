import os
import time
import ntplib
import socket
import configparser


class DebugPrint(object):
    def __init__(self, enabled):
        self.enabled = enabled

    def __call__(self, msg):
        if self.enabled:
            t = time.localtime()
            current_time = time.strftime("[%H:%M:%S] ", t)
            print(current_time + str(msg))


class Internet():
    def __init__(self, device_name):
        self.device_name = device_name

    def connect(self, reconnect_attepmts=10, waiting_seconds=5, force_internet=False):
        # print('Connecting to internet via "{}" device'.format(self.device_name))
        # LOG.warning('Connecting to internet via "{}" device'.format(self.device_name))
        check_connection_code = os.system('nmcli dev connect {}'.format(self.device_name))
        # Connection code for connected = 0, 2560 when no connection, 1024 when problems of connecting
        if force_internet is True:
            for x in range(reconnect_attepmts):
                if check_connection_code == 0:
                    break
                else:
                    os.system('nmcli dev disconnect {}'.format(self.device_name))
                    time.sleep(waiting_seconds)
                    print('Trying to reconnect to 4G Dongle. Attempt: {}...'.format(x + 1))
                    check_connection_code = os.system('nmcli dev connect {}'.format(self.device_name))
            if check_connection_code != 0:
                print("Could not connect to 4G Dongle after {0} attempts. Timed out after {1} seconds.".format(
                    reconnect_attepmts, reconnect_attepmts * waiting_seconds))
                return False
        time.sleep(1)
        return True

    def disconnect(self):
        # print('Disconnecting from internet via "{}" device'.format(self.device_name))
        # LOG.warning('Disconnecting from internet via "{}" device'.format(self.device_name))
        os.system('nmcli dev disconnect {}'.format(self.device_name))

    def update_time(self, reconnect_attepmts=10, waiting_seconds=2, force_internet=False):
        status = False
        try:
            client = ntplib.NTPClient()
            response = client.request('pool.ntp.org')
            os.system('date ' + time.strftime('%m%d%H%M%Y.%S', time.localtime(response.tx_time)))
            status = True
        except (socket.gaierror, ntplib.NTPException) as err:
            status = False

        if force_internet is True:
            for x in range(reconnect_attepmts):
                if status is False:
                    try:
                        print("'Trying to connect the internet and update time.. Attempt: {}".format(x + 1))
                        client = ntplib.NTPClient()
                        response = client.request('pool.ntp.org')
                        os.system('date ' + time.strftime('%m%d%H%M%Y.%S', time.localtime(response.tx_time)))
                        status = True
                    except (socket.gaierror, ntplib.NTPException) as err:
                        status = False
                        time.sleep(waiting_seconds)
                if status is True:
                    print('Succesfully connected to the internet and updated time.')
                    break
            if status is False:
                print("Could not connect to 4G Dongle after {0} attempts. Timed out after {1} seconds.".format(
                    reconnect_attepmts, reconnect_attepmts * waiting_seconds))
                return False
        return status


class Config():
    def __init__(self, cfg_relative_path='../config/config.ini'):
        self.cfg_path = cfg_relative_path
        self.root_path = os.path.dirname(os.path.abspath(__file__))
        self.full_path = os.path.join(self.root_path, self.cfg_path)

    # READ CONFIG VALUES AND STORE THEM
    def init_config(self):
        # Check if config file exists. If not -> create new file.
        if not os.path.exists(self.full_path) or os.stat(self.full_path).st_size == 0:
            raise SystemError('config file is not found at {}'.format(self.full_path))

        config = configparser.ConfigParser()
        config.read(self.full_path)
        return config


NOC = Config()
CONFIG_SETTINGS = NOC.init_config()

# Initializing internet connection
SETTINGS = CONFIG_SETTINGS['main']
INTERNET_DEVICE_NAME = SETTINGS['INTERNET_DEVICE_NAME']
INT = Internet(INTERNET_DEVICE_NAME)
