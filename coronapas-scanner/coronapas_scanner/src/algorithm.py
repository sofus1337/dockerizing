import time
import datetime
import os

import requests

from utils import DebugPrint, CONFIG_SETTINGS
from logger import LOG
from qr_reader import qr_decode_img
from coronapas_decode import simple_validator, read_coronapas

DEBUG_SAVE_IMAGES = False

DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)

SETTINGS = CONFIG_SETTINGS['common']
IMX_OR_REALSENSE = SETTINGS.getint('IMX_OR_REALSENSE')
# Only of the two can be used, but still importing both
if IMX_OR_REALSENSE == 0:  # IMX:
    from imx219 import IMX219
elif IMX_OR_REALSENSE == 1:  # RealSense
    from realsense import RealSense
    from qr_reader import RS_enhance_image

LABEL_PRINTER_ENABLED = SETTINGS.getboolean('LABEL_PRINTER_ENABLED')
if LABEL_PRINTER_ENABLED is True:
    from ticket_printer import LabelPrinter
    from qr_reader import make_qrcode

DEBUG_MAIN_LOOP_TIME_ENABLED = SETTINGS.getboolean('DEBUG_MAIN_LOOP_TIME_ENABLED')
if DEBUG_MAIN_LOOP_TIME_ENABLED is True:
    from apscheduler.schedulers.background import BackgroundScheduler


class CoronapasScanner(object):
    def __init__(self, host_ip):
        self.HOST = host_ip

        #  Config parameters
        # ===============================================================================================
        self.JETSON_HARDWARE_VERSION = CONFIG_SETTINGS['main']['JETSON_HARDWARE_VERSION']  # string '2GB' or '4GB'
        self.SCAN_COUNTING_ENABLED = SETTINGS.getboolean('SCAN_COUNTING_ENABLED')
        self.REBOOT_TIMER_MAX = SETTINGS.getint('REBOOT_TIMER_MAX')
        self.RESET_MAX = SETTINGS.getint('RESET_MAX')
        self.BARCODE_MIN_SIZE_RATIO_TO_IMG = SETTINGS.getfloat('BARCODE_MIN_SIZE_RATIO_TO_IMG')
        _qrcode_size = SETTINGS.getint('CORONAPAS_QRCODE_SIZE')
        self.CORONAPAS_QRCODE_SIZE = (_qrcode_size, _qrcode_size)
        self.TIMER_IDLE_SCREEN_DELAY = SETTINGS.getfloat('TIMER_IDLE_SCREEN_DELAY')
        self.TIMER_RESULT_RED_SCREEN = SETTINGS.getfloat('TIMER_RESULT_RED_SCREEN')
        self.TIMER_RESULT_GREEN_SCREEN = SETTINGS.getfloat('TIMER_RESULT_GREEN_SCREEN')
        self.TIMER_RESULT_YELLOW_SCREEN = SETTINGS.getfloat('TIMER_RESULT_YELLOW_SCREEN')
        self.TIMER_RESULT_PRINTER_SCREEN = SETTINGS.getfloat('TIMER_RESULT_PRINTER_SCREEN')
        self.EXPIRE_OFFSET = SETTINGS.getint('EXPIRE_OFFSET')

        # Class members
        # ===============================================================================================
        self.qr_decode_img_looptime = 0  # seconds 'qr_decode_img' function runs. Variable is filled after 1 program loop.
        self.timer_reboot = 100000000000  # a number bigger than time.time() time will ever be.
        self.first_run_reboot = False
        self.first_run_fail_cnt = 0
        self.first_run_flag = True
        self.initialized = False
        self.reset_cnt = 0
        self.good_frame = False  # used by ping_camera()
        self.hardware_status_healthy = True  # turns 'False' if no connection to some HW
        self.RS = None  # Realsense camera
        self.LP = None  # Brother Label Printer
        self.camera_enabled = False  # 'init_modules' and 'ping_camera' status variable.

        if DEBUG_MAIN_LOOP_TIME_ENABLED is True:
            self.main_loop_time = 0

            def print_main_loop_time():
                DEBUG('Main Loop time is: {:1.2f} seconds'.format(self.main_loop_time))
                LOG.critical('Main Loop time is: {:1.2f} seconds'.format(self.main_loop_time))

            scheduler = BackgroundScheduler()
            scheduler.add_job(print_main_loop_time, 'cron', minute='0,5,10,15,20,25,30,35,40,45,50,55')
            # scheduler.add_job(self.print_main_loop_time, 'cron', minute='0-59')
            scheduler.start()

        self.outer_loop()

    def init_modules(self):
        t1_init_modules = time.time()

        if not self.camera_enabled:
            if IMX_OR_REALSENSE == 0:  # IMX = 0
                try:
                    if hasattr(self, 'IMX'):
                        del self.IMX
                    self.IMX = IMX219()
                    check_imx = self.IMX.get_capture_object()
                    if check_imx is None:
                        return False
                    test_frame = self.IMX.getframe()
                    if test_frame is not None:
                        LOG.warning('IMX219 Camera - CONNECTED.')
                        DEBUG('IMX219 Camera - CONNECTED.')
                        self.camera_enabled = True
                    else:
                        LOG.warning('IMX219 Camera - NOT CONNECTED.')
                        DEBUG('IMX219 Camera - NOT CONNECTED.')
                        return False
                except RuntimeError:
                    LOG.critical('IMX219 Camera - NOT CONNECTED.')
                    DEBUG('IMX219 Camera - NOT CONNECTED.')
                    return False
            elif IMX_OR_REALSENSE == 1:  # Realsense = 1
                try:
                    self.RS = RealSense(NO_DEPTH=True)
                    LOG.warning('RealSense Camera - CONNECTED.')
                    DEBUG('RealSense Camera - CONNECTED.')
                    self.camera_enabled = True
                except RuntimeError:
                    LOG.critical('RealSense Camera - NOT CONNECTED.')
                    DEBUG('RealSense Camera - NOT CONNECTED.')
                    return False

        if LABEL_PRINTER_ENABLED is True:
            self.LP = LabelPrinter()

        t2_init_modules = time.time()
        t_init_modules_diff = t2_init_modules - t1_init_modules
        if t_init_modules_diff > 0.1:  # to avoid printing 0.000 when nothing is executed here
            LOG.warning('Initialisation time: {:1.2f} seconds'.format(t_init_modules_diff))
            DEBUG('Initialisation time: {:1.2f} seconds'.format(t_init_modules_diff))
        return True

    def ping_camera(self):
        if IMX_OR_REALSENSE == 0:  # 0 - IMX
            try:
                imx_frame = self.IMX.getframe()
                if imx_frame is None:
                    LOG.critical('IMX Camera - CANNOT RECEIVE FRAMES')
                    DEBUG('IMX Camera - CANNOT RECEIVE FRAMES')
                    self.camera_enabled = False
                    return False
            except RuntimeError:
                LOG.critical('IMX Camera - NOT CONNECTED')
                DEBUG('IMX Camera - NOT CONNECTED')
                self.camera_enabled = False
                return False
            return True
        elif IMX_OR_REALSENSE == 1:  # 1 - Realsense
            try:
                rs_status = self.RS.ping()
                if rs_status is False:
                    LOG.critical('RealSense Camera - CANNOT RECEIVE FRAMES')
                    DEBUG('RealSense Camera - CANNOT RECEIVE FRAMES')
                    self.camera_enabled = False
                    return False
            except RuntimeError:
                LOG.critical('RealSense Camera - NOT CONNECTED')
                DEBUG('RealSense Camera - NOT CONNECTED')
                self.camera_enabled = False
                return False
            return True

    def send_app_request(self, emit_signal):
        try:
            # DEBUG("[TABLET] -> {} ".format(emit_signal))
            requests.get(self.HOST + emit_signal)
        except requests.exceptions.ConnectionError as err:
            err_string = 'Error: {}.'.format(err)
            err_string2 = 'No connection to App Server. Terminating the program.'
            LOG.critical(err_string)
            LOG.critical(err_string2)
            raise SystemError(err_string2)

    def count_scan_generate_default(self):
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        dir_name = os.uname()[1]
        rel_path = 'log/{}'.format(dir_name)
        dir_path = os.path.join(base_path, rel_path)
        file_name = '{}_scans.log'.format(dir_name)
        full_path = os.path.join(base_path, dir_path, file_name)
        DEBUG('Creating new file...')
        default_txt = {
            'scanner_id': os.uname()[1],  # hostname
            'last_updated': str(datetime.datetime.now()),
            'coronapas_valid_scan': 0,
            'coronapas_invalid_scan': 0,
            'coronapas_expired_scan': 0,
            'coronapas_would_be_expired_scan_drift': 0,
            'coronapas_would_be_expired_scan_extra_time': 0,
            'total_scans': 0,
        }
        with open(full_path, 'w') as f:
            for key, value in default_txt.items():
                f.write('%s:%s\n' % (key, value))

    def count_scan(self, coronapas=None):
        base_path = os.path.dirname(os.path.dirname(__file__))
        dir_name = os.uname()[1]
        rel_path = 'log/{}'.format(dir_name)
        dir_path = os.path.join(base_path, rel_path)
        file_name = '{}_scans.log'.format(dir_name)
        full_path = os.path.join(base_path, dir_path, file_name)

        if not os.path.isdir(dir_path):
            os.mkdir(dir_path)

        if not os.path.isfile(full_path):
            DEBUG('File does not exist at {}'.format(full_path))
            self.count_scan_generate_default()

        scan_info = {}
        try:
            with open(full_path, 'r') as f:
                for line in f:
                    (key, val) = line.strip().split(':', 1)
                    scan_info[key] = val

            for key, value in scan_info.items():
                if value.isdecimal() is True:
                    scan_info[key] = int(value)
        except ValueError as e:
            DEBUG(e)
            LOG.warning(e)
            LOG.warning('Content of scan count txt is wrong. Here is what was removed:')
            with open(full_path, 'r') as f:
                for line in f:
                    LOG.warning(line)
            open(full_path, 'w').close()

            self.count_scan_generate_default()

            with open(full_path, 'r') as f:
                for line in f:
                    (key, val) = line.strip().split(':', 1)
                    scan_info[key] = val
            for key, value in scan_info.items():
                if value.isdecimal() is True:
                    scan_info[key] = int(value)

        scan_info['scanner_id'] = os.uname()[1]  # get hostname (Coronapas Scanner unique ID)
        scan_info['last_updated'] = str(datetime.datetime.now())  # get current timedate
        scan_info['total_scans'] += 1  # Iterate total # of scans by 1
        try:
            if coronapas is not None:
                if coronapas == 0:
                    scan_info['coronapas_valid_scan'] += 1
                elif coronapas == 1:
                    scan_info['coronapas_invalid_scan'] += 1
                elif coronapas == 2:
                    scan_info['coronapas_expired_scan'] += 1
                elif coronapas == 3:
                    scan_info['coronapas_would_be_expired_scan_drift'] += 1
                elif coronapas == 4:
                    scan_info['coronapas_would_be_expired_scan_extra_time'] += 1
                else:
                    DEBUG('Error in count_scans() -> Wrong "coronapas" string: {}'.format(coronapas))
        except KeyError as e:
            DEBUG(e)
            LOG.critical(e)
            self.count_scan_generate_default()
            return 0

        with open(full_path, 'w') as f:
            for key, value in scan_info.items():
                f.write('%s:%s\n' % (key, value))

    def outer_loop(self):
        """Outer loop of Coronapas Scanner algorithm. Initialized HW/SW and checks status.
           If status is OK, goes through connection check and if ok, launches main algorithm.
        """
        if LABEL_PRINTER_ENABLED is False:
            DEBUG('Sofware name: Coronapas Scanner.')
            LOG.critical('Sofware name: Coronapas Scanner.')
        else:
            DEBUG('Sofware name: Coronapas Scanner with Ticket Printer.')
            LOG.critical('Sofware name: Coronapas Scanner with Ticket Printer.')

        DEBUG('Jetson architecture version: {}'.format(self.JETSON_HARDWARE_VERSION))
        LOG.warning('Jetson architecture version: {}'.format(self.JETSON_HARDWARE_VERSION))

        while True:
            if self.first_run_reboot:
                self.timer_reboot = time.time()
                self.first_run_reboot = False

            timer_reboot_diff = time.time() - self.timer_reboot
            if self.initialized is False and timer_reboot_diff > self.REBOOT_TIMER_MAX:
                self.send_app_request('detection_idle')
                DEBUG('Quitting the program. Initialization failed for {} seconds.'.format(self.REBOOT_TIMER_MAX))
                LOG.critical('Doing system reboot. Initialization failed for {} seconds.'.format(
                    self.REBOOT_TIMER_MAX))
                raise SystemError('Program terminated. Awaiting PID checker to catch it and start a reboot.')
            self.initialized = self.init_modules()  # 'True' if all good.

            if self.initialized is False and self.first_run_flag is True:
                self.first_run_fail_cnt += 1

            if self.first_run_fail_cnt == 1:
                self.first_run_flag = False
                self.first_run_fail_cnt = 0
                self.first_run_reboot = True

            while self.initialized:
                self.first_run_reboot = True
                if self.reset_cnt >= self.RESET_MAX:
                    self.initialized = False
                    self.reset_cnt = 0
                    break
                self.good_frame = self.ping_camera()
                self.reset_cnt += 1
                if self.good_frame is True:
                    self.hardware_status_healthy = True
                    LOG.warning('Program initialized successfully.\n')
                    DEBUG('Program initialized successfully.\n')
                    self.reset_cnt = 0
                    self.main_loop()
                else:
                    LOG.critical('Hardware initialization failed.')
                    DEBUG('Hardware initialization failed.')
                    time.sleep(1)

    def main_loop(self):
        self.send_app_request('coronapass_begin')
        while self.hardware_status_healthy:

            if DEBUG_MAIN_LOOP_TIME_ENABLED is True:
                t1_main_loop_time = time.time()

            # t1_getframe = time.time()
            if IMX_OR_REALSENSE == 0:  # IMX
                img_color = self.IMX.getframe()
            elif IMX_OR_REALSENSE == 1:  # Realsense
                img_color = self.RS.getframe_rgb()
            # t2_getframe = time.time()

            if img_color is None:
                DEBUG('Camera frame is None. Jumping out of main loop.')
                LOG.critical('Camera frame is None. Jumping out of main loop.')
                self.hardware_status_healthy = False
                break

            enhance_status = None
            # t1_enhance = time.time()
            if IMX_OR_REALSENSE == 0:  # IMX
                enh_img_color, enhance_status = self.IMX.enhance_image(img_color, qr_decode_runtime=self.qr_decode_img_looptime)
            elif IMX_OR_REALSENSE == 1:  # Realsense
                enh_img_color, enhance_status = RS_enhance_image(img_color)
            # t2_enhance = time.time()

            t1_decode = time.time()
            qrcode = qr_decode_img(enh_img_color, save_img=DEBUG_SAVE_IMAGES, enhance_comment=enhance_status)
            t2_decode = time.time()
            self.qr_decode_img_looptime = t2_decode - t1_decode
            # print(self.qr_decode_img_looptime)
            # NOTE code below will take original image and scan it for qr codes if enhanced returned nothing.
            # WARNING: triples the cycle time!
            # if not qrcode:
            #    qrcode = qr_decode_img(img_color, save_img=DEBUG_SAVE_IMAGES)
            if qrcode:
                if len(qrcode) == 0:  # no qr code detected
                    pass
                if len(qrcode) > 1:  # if multiple qr codes are present
                    DEBUG('2 qr codes found, taking largest one')
                    qr_sizes = []
                    for qr in qrcode:
                        [x, y] = qr[2]
                        size = qr[3]
                        qr_sizes.append(size)
                    largest_size_idx = qr_sizes.index(max(qr_sizes))
                    qrcode = qrcode[largest_size_idx]
                    qrcode = [qrcode]
                if len(qrcode) == 1:
                    qr_string, qr_type, [x, y], qr_size_ratio_to_img = qrcode[0]
                    # QR-code ratio to image size.
                    # 1 = qr code is one-to-one of image size.
                    # 0 = qr code occupies no pixels in image.
                    # 0.5 = qr code occupies exactly HALF pixels on the image
                    if qr_size_ratio_to_img < self.BARCODE_MIN_SIZE_RATIO_TO_IMG:
                        # NOTE important 'pass'!
                        # Ignores barcodes smaller than set threshold of "BARCODE_MIN_SIZE_RATIO_TO_IMG"
                        pass
                    elif qr_type != 'QRCODE':  # Coronapas are of 'QRCODE' type only, so far.
                        DEBUG('Detected barcode is not of QRCODE type. It is {0}. Size {1:1.2f}'.format(
                            qr_type, qr_size_ratio_to_img))
                        LOG.warning('Detected barcode is not of QRCODE type. It is {0}. Size {1:1.2f}'.format(
                            qr_type, qr_size_ratio_to_img))
                    else:
                        result = read_coronapas(qr_string)
                        if result is None:
                            DEBUG('Coronapas: Invalid/Unknown. Size {:1.2f}'.format(qr_size_ratio_to_img))
                            LOG.warning('Coronapas: Invalid/Unknown. Size {:1.2f}'.format(qr_size_ratio_to_img))
                            self.send_app_request('coronapass_unknown')  # RED SCREEN
                            if self.SCAN_COUNTING_ENABLED is True:
                                self.count_scan(coronapas=1)  # 1 is code for "Invalid"
                            time.sleep(self.TIMER_RESULT_RED_SCREEN)
                        else:
                            valid, count_scan_status = simple_validator(result, EXPIRE_OFFSET=self.EXPIRE_OFFSET)
                            # self.send_app_request('coronapass_validate')
                            # DEBUG('Sleeping 1 sec to see animation')
                            # time.sleep(1)  # To see the green animation
                            if self.SCAN_COUNTING_ENABLED is True:
                                self.count_scan(coronapas=count_scan_status)

                            if IMX_OR_REALSENSE == 0:  # IMX:
                                DEBUG('QR-Code Size {0:1.2f}. Image enhancements: {1}'.format(
                                    qr_size_ratio_to_img, enhance_status))
                                LOG.warning('QR-Code Size {0:1.2f}. Image enhancements: {1}'.format(
                                    qr_size_ratio_to_img, enhance_status))
                            elif IMX_OR_REALSENSE == 1:  # RealSense
                                DEBUG('QR-Code Size {:1.2f}'.format(qr_size_ratio_to_img))
                                LOG.warning('QR-Code Size {:1.2f}'.format(qr_size_ratio_to_img))

                            printer_available = None
                            if LABEL_PRINTER_ENABLED is True:
                                qr_img = make_qrcode(qr_string)
                                resized_qr_img = qr_img.resize(self.CORONAPAS_QRCODE_SIZE)
                                status = self.LP.print_ticket(valid, result['valid_to'], qr_img=resized_qr_img)
                                if status is True:
                                    printer_available = True
                                else:
                                    printer_available = False

                            if printer_available is True:
                                self.send_app_request('temperature_measured_good_printer')
                                time.sleep(self.TIMER_RESULT_PRINTER_SCREEN)
                            else:
                                if valid is True:
                                    self.send_app_request('coronapass_valid')
                                    time.sleep(self.TIMER_RESULT_GREEN_SCREEN)
                                else:
                                    self.send_app_request('coronapass_invalid')  # UDLØBET! YELLOW SCREEN
                                    time.sleep(self.TIMER_RESULT_YELLOW_SCREEN)

                        self.send_app_request('detection_idle')
                        time.sleep(self.TIMER_IDLE_SCREEN_DELAY)
                        self.send_app_request('coronapass_begin')

            if DEBUG_MAIN_LOOP_TIME_ENABLED is True:
                self.main_loop_time = time.time() - t1_main_loop_time

            # os.system('clear')
            # DEBUG('Main loop time: \t {:1.2f}sec'.format(self.main_loop_time))
            # DEBUG('Get frame time: \t {:1.4f}sec'.format(t2_getframe - t1_getframe))
            # DEBUG('Enhance   time: \t {:1.4f}sec'.format(t2_enhance - t1_enhance))
            # DEBUG('Decode QR time: \t {:1.2f}sec'.format(t2_decode - t1_decode))
