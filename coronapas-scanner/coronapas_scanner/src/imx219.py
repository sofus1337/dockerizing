import cv2
import time
import threading
import queue
import random
import numpy as np

from utils import CONFIG_SETTINGS, DebugPrint
from logger import LOG

DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)

SETTINGS = CONFIG_SETTINGS['imx']


class IMX219(object):

    IMX_IMG_WIDTH = SETTINGS.getint('IMX_IMG_WIDTH')
    IMX_IMG_HEIGHT = SETTINGS.getint('IMX_IMG_HEIGHT')
    IMX_FRAMERATE = SETTINGS.getint('IMX_FRAMERATE')
    IMX_FLIP_METHOD = SETTINGS.getint('IMX_FLIP_METHOD')
    IMX_CONNECTION_TIMEOUT = SETTINGS.getint('IMX_CONNECTION_TIMEOUT')
    IMX_SLEEP_CAMERA_INIT = SETTINGS.getfloat('IMX_SLEEP_CAMERA_INIT')

    binary_img_count = 0
    dark_frame_count = 0
    light_frame_count = 0
    dark_mode = False
    light_mode = True
    use_binary_img = True

    GREYSCALE_FRAME_COUNT_TO_SWITCH_MODES = SETTINGS.getint('GREYSCALE_FRAME_COUNT_TO_SWITCH_MODES')
    QR_DECODE_RUNTIME_THRESHOLD = SETTINGS.getfloat('QR_DECODE_RUNTIME_THRESHOLD')
    BINARY_IMG_MAX = SETTINGS.getint('BINARY_IMG_MAX')

    class VideoCaptureDaemon(threading.Thread):
        def __init__(self, video, result_queue):
            super().__init__()
            self.daemon = True
            self.video = video
            self.result_queue = result_queue

        def run(self):
            self.result_queue.put(cv2.VideoCapture(self.video, cv2.CAP_GSTREAMER))

    def __init__(self):
        self.pipeline = self._gstreamer_pipeline()
        self.cap = self.get_video_capture(self.pipeline)
        # self.cap = cv2.VideoCapture(self.pipeline, cv2.CAP_GSTREAMER)
        time.sleep(3)

    def get_capture_object(self):
        return self.cap

    def get_video_capture(self, video):
        res_queue = queue.Queue()
        self.VideoCaptureDaemon(video, res_queue).start()
        try:
            return res_queue.get(block=True, timeout=self.IMX_CONNECTION_TIMEOUT)
        except queue.Empty as e:
            DEBUG(e)
            LOG.critical(e)
            err_msg = 'cv2.VideoCapture: could not grab input ({}). Timeout occurred after {:.2f}s'.format(
                video, self.IMX_CONNECTION_TIMEOUT)
            DEBUG(err_msg)
            LOG.critical(err_msg)
            return None

    def _gstreamer_pipeline(self):
        return (
            "nvarguscamerasrc ! "
            "video/x-raw(memory:NVMM), "
            "width=(int)%d, height=(int)%d, "
            "format=(string)NV12, framerate=(fraction)%d/1 ! "
            "nvvidconv flip-method=%d ! "
            "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
            "videoconvert ! "
            "video/x-raw, format=(string)BGR ! appsink max-buffers=1 drop=True"
            % (
                self.IMX_IMG_WIDTH,
                self.IMX_IMG_HEIGHT,
                self.IMX_FRAMERATE,
                self.IMX_FLIP_METHOD,
                self.IMX_IMG_WIDTH,
                self.IMX_IMG_HEIGHT,
            )
        )

    def getframe(self):
        try:
            if self.cap.isOpened():
                ret, img = self.cap.read()
                if ret is True:
                    return img
                else:
                    return None  # False
        except RuntimeError:
            DEBUG('RuntimeError: No connection to camera.')
            LOG.critical('RuntimeError: No connection to camera.')
            return None

    def enhance_image(self, src_img, qr_decode_runtime=None):

        if qr_decode_runtime is not None:
            # check and changes variable states for light/dark modes. NOTE: takes only greyscale!
            self.night_or_day_mode_tracker(qr_decode_runtime)

        src_img = src_img[50:670, 200:1080]  # Cropping. Must know src image resolution! (current is for 720x1280)
        src_img = np.float32(src_img[:, :, 0])  # Extracting grey channel (0th) from 'YUV' format. Y - greyscale
        enhance_status = 'g'
        self.use_binary_img = True
        # controlling sequence where for every 'BINARY_IMG_MAX' number of binary images, 1 will be just greyscale
        if (self.binary_img_count == self.BINARY_IMG_MAX):
            self.binary_img_count = 0
            self.use_binary_img = False
        # converting greyscale to binary using different parameters, depending if its light or dark environment on image
        if self.use_binary_img is True:
            if self.dark_mode is True:
                choices = [[-0.301, 26], [-0.15, -15]]
                [alpha, beta] = random.choice(choices)
                binary_thres = 50
                for idx, pair in enumerate(choices):
                    if [alpha, beta] == pair:
                        if idx == 0:
                            enhance_status = 'bdm1'  # '1' means first pair, which is [-0.301, 26]
                    elif idx == 1:
                            enhance_status = 'bdm2'  # '2' means second pair, which is [-0.15, -15]
            else:
                alpha, beta = -0.8, 100
                binary_thres = 100
                enhance_status = 'blm'
            src_img = cv2.convertScaleAbs(src_img, alpha=alpha, beta=beta)  # Changing contract and brightness
            ret, src_img = cv2.threshold(src_img, binary_thres, 255, cv2.THRESH_BINARY)
            self.binary_img_count += 1
        return src_img, enhance_status

    def night_or_day_mode_tracker(self, qr_decode_runtime):
        # qr_decode_looptime: Float, running time in seconds of qr_decode_img() function

        if self.use_binary_img is False:
            if qr_decode_runtime > self.QR_DECODE_RUNTIME_THRESHOLD:
                self.dark_frame_count += 1  # increment dark mode counter
                self.light_frame_count = 0  # while resetting light mode counter
            else:
                self.light_frame_count += 1  # increment light mode counter
                self.dark_frame_count = 0    # while resetting light mode counter

            if self.dark_frame_count == self.GREYSCALE_FRAME_COUNT_TO_SWITCH_MODES:
                self.dark_mode = True
                self.light_mode = False
                DEBUG("Enabling Night Mode")
            if self.light_frame_count == self.GREYSCALE_FRAME_COUNT_TO_SWITCH_MODES:
                self.light_mode = True
                self.dark_mode = False
                DEBUG("Enabling Day Mode")

    def __del__(self):
        # checking for None to present cap.release() method error when cap = None, set in get_video_capture() method.
        if hasattr(self, 'cap') and self.cap is not None:
            self.cap.release()


# Demo of camera. Initializes camera pipeline, takes image, shows it in a window. Press any key to quit.
if __name__ == "__main__":
    try:
        # for x in range(3):
        #     CAM = IMX219()
        #     time.sleep(1)
        #     print('ae')
        # my_img = CAM.getframe()
        # my_img = my_img[:, :, 0]
        # cv2.waitKey()
        # cv2.imshow("my_img", my_img)
        # time.sleep(1)
        # if 'CAM' in globals():
        #     del CAM
        #     print('aee')
        #     time.sleep(1)

        pass
        # my_img = CAM.getframe()
        # my_img = my_img[:, :, 0]
        # cv2.imshow("my_img", my_img)
        # cv2.waitKey()
        # cv2.destroyAllWindows()
    except KeyboardInterrupt:
        quit()
