import queue
import flask
import sys
import os
import time
import traceback

from utils import DebugPrint, CONFIG_SETTINGS
from logger import LOG

DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)

SETTINGS = CONFIG_SETTINGS['main']
JETSON_HARDWARE_VERSION = SETTINGS['JETSON_HARDWARE_VERSION']  # string '2GB' or '4GB'
if JETSON_HARDWARE_VERSION == '2GB':
    import subprocess
    WIFI_DONGLE_DEVICE_NAME = SETTINGS['WIFI_DONGLE_DEVICE_NAME']
    WIFI_DONGLE_ATTEMPTS = SETTINGS.getint('WIFI_DONGLE_ATTEMPTS')
    WIFI_DONGLE_SLEEP_INTERVAL = SETTINGS.getfloat('WIFI_DONGLE_SLEEP_INTERVAL')

SETTINGS = CONFIG_SETTINGS['main']
IP_ADDRESS = SETTINGS['IP_ADDRESS']
PORT = SETTINGS['PORT']
HOST = 'http://{0}:{1}/'.format(IP_ADDRESS, PORT)

app = flask.Flask(__name__)


class MessageAnnouncer:
    def __init__(self):
        self.listeners = []

    def listen(self):
        self.listeners.append(queue.Queue(maxsize=5))
        return self.listeners[-1]

    def announce(self, msg):
        # We go in reverse order because we might have to delete an element, which will shift the
        # indices backward
        for i in reversed(range(len(self.listeners))):
            try:
                self.listeners[i].put_nowait(msg)
            except queue.Full:
                del self.listeners[i]


announcer = MessageAnnouncer()


def format_sse(data: str, event=None) -> str:
    """Formats a string and an event name in order to follow the event stream convention.
    >>> format_sse(data=json.dumps({'abc': 123}), event='Jackson 5')
    'event: Jackson 5\\ndata: {"abc": 123}\\n\\n'
    """
    msg = f'data: {data}\n\n'
    if event is not None:
        msg = f'event: {event}\n{msg}'
    return msg


@app.route('/detection_idle')
def detection_idle():
    msg = format_sse(data='detection_idle')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/ping')
def ping():
    msg = format_sse(data='pong')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/reboot')
def reboot():
    msg = format_sse(data='reboot')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/listen', methods=['GET'])
def listen():

    def stream():
        messages = announcer.listen()  # returns a queue.Queue
        while True:
            msg = messages.get()  # blocks until a new message arrives
            yield msg

    return flask.Response(stream(), mimetype='text/event-stream')


@app.route('/coronapass_begin')
def coronapas():
    msg = format_sse(data='coronapass_begin')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/coronapass_validate')
def coronapas_validate():
    msg = format_sse(data='coronapass_validate')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/temperature_measured_good_printer')
def temperature_measured_good_printer():
    msg = format_sse(data='temperature_measured_good_printer')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/coronapass_valid')
def coronapass_valid():
    msg = format_sse(data='coronapass_valid')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/coronapass_invalid')
def coronapass_invalid():
    msg = format_sse(data='coronapass_invalid')
    announcer.announce(msg=msg)
    return {}, 200


@app.route('/coronapass_unknown')
def coronapass_unknown():
    msg = format_sse(data='coronapass_unknown')
    announcer.announce(msg=msg)
    return {}, 200


if __name__ == '__main__':
    try:
        if JETSON_HARDWARE_VERSION == '2GB':
            wifi_dongle_enabled = False
            for x in range(WIFI_DONGLE_ATTEMPTS + 1):
                DEBUG('Attempt {} to find Wifi Dongle...'.format(x + 1))
                LOG.critical('Attempt {} to find Wifi Dongle...'.format(x + 1))
                connected_usb_devices = subprocess.check_output('lsusb')
                if WIFI_DONGLE_DEVICE_NAME in str(connected_usb_devices):
                    wifi_dongle_enabled = True
                    LOG.critical('WiFi dongle - Connected.')
                    DEBUG('WiFi dongle - Connected.')
                if wifi_dongle_enabled is True:
                    break
                elif (x + 1) >= WIFI_DONGLE_ATTEMPTS:
                    LOG.critical('WiFi Dongle - Not found in connected USD devices. Name: {}'.format(
                        WIFI_DONGLE_DEVICE_NAME))
                    raise Exception('WiFi Dongle - Not found in connected USD devices. Name: {}'.format(
                        WIFI_DONGLE_DEVICE_NAME))
                time.sleep(WIFI_DONGLE_SLEEP_INTERVAL)

        DEBUG('Starting server at http://{0}:{1}'.format(IP_ADDRESS, PORT))
        app.run(host=IP_ADDRESS, port=PORT, debug=True, threaded=True)
    except Exception:
        exc_type = sys.exc_info()[0]
        DEBUG(traceback.format_exc())
        LOG.critical(traceback.format_exc())
        DEBUG('App Server failed to start. Rebooting in 5 seconds.')
        LOG.critical('App Server failed to start. Rebooting in 5 seconds.')
        time.sleep(5)
        os.system('sudo reboot')
