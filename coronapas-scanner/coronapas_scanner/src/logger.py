import os
import logging
from logging.handlers import RotatingFileHandler
from utils import CONFIG_SETTINGS


class Logger():
    def __init__(self, log_path):
        self.log_path = log_path

    def init_logger(self, LOG_FILE_MAX_SIZE_MB=200):
        """Logger for Coronapas Scanner. Saves log history in one file, and ongoingly writes log to the other file.
        'LOG_FILE_MAX_SIZE_MB' variable controls how many megabytes of log data to store.
        Uses 'utf-8' encoding to allow for special characters: rød grød med fløde!

        Returns:
        app_log: logger handle to use for logging. Example: app_log.critical('Critical message!')
        """
        log_formatter = logging.Formatter('%(asctime)s :: %(message)s')
        # log_formatter = logging.Formatter('%(message)s')
        path = os.path.dirname(os.path.abspath(__file__))
        logFile = os.path.join(path, self.log_path)

        my_handler = RotatingFileHandler(logFile,
                                         mode='a',
                                         maxBytes=LOG_FILE_MAX_SIZE_MB * 1024 * 1024,
                                         backupCount=1,
                                         encoding='utf-8',
                                         delay=0)
        my_handler.setFormatter(log_formatter)
        my_handler.setLevel(logging.WARNING)

        app_log = logging.getLogger('root')
        app_log.setLevel(logging.WARNING)
        app_log.addHandler(my_handler)
        return app_log


# Initializing config settings as "SETTINGS" object
SETTINGS = CONFIG_SETTINGS['main']

# Absolute path to this file.
base_path = os.path.dirname(os.path.abspath(__file__))

# Initializing Logger as 'LOG' object
branch_name = os.uname()[1]
LOG_DIR_SHORT = '../log/{}'.format(branch_name)
LOG_DIR = os.path.join(base_path, LOG_DIR_SHORT)
LOG_FILE = '{}_systemlog.log'.format(branch_name)
LOG_PATH = os.path.join(LOG_DIR, LOG_FILE)
LOG_FILE_MAX_SIZE_MB = SETTINGS.getint('LOG_FILE_MAX_SIZE_MB')
if os.path.isdir(LOG_DIR):
    LOGGER = Logger(LOG_PATH)
else:
    os.mkdir(LOG_DIR)
    LOGGER = Logger(LOG_PATH)


LOG = LOGGER.init_logger(LOG_FILE_MAX_SIZE_MB=LOG_FILE_MAX_SIZE_MB)
