from typing import Union
import time
import cv2
import numpy as np
import requests
import zlib
import cbor2
import random

from cose.messages import CoseMessage
from utils import CONFIG_SETTINGS, DebugPrint
from qr_reader import IMX_enhance_image, qr_decode_img
from logger import LOG
from imx219 import IMX219

DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)

SETTINGS = CONFIG_SETTINGS['main']
IP_ADDRESS = SETTINGS['IP_ADDRESS']
PORT = SETTINGS['PORT']
HOST = 'http://{0}:{1}/'.format(IP_ADDRESS, PORT)


def send_app_request(emit_signal):
    try:
        # DEBUG("[TABLET] -> {} ".format(emit_signal))
        requests.get(HOST + emit_signal)
    except requests.exceptions.ConnectionError as err:
        err_string = 'Error: {}.'.format(err)
        err_string2 = 'No connection to App Server. Terminating the program.'
        DEBUG(err_string)
        DEBUG(err_string2)
        raise SystemError(err_string2)


def b45decode(s: Union[bytes, str]) -> bytes:
    """Decode base45-encoded string to bytes.
        Taken from https://github.com/ehn-digital-green-development/ehn-sign-verify-python-trivial
        Under EUPL licence.
    """

    BASE45_CHARSET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:"

    res = []
    try:
        if isinstance(s, str):
            buf = [BASE45_CHARSET.index(c) for c in s]
        else:
            buf = [BASE45_CHARSET.index(c) for c in s.decode()]
        buflen = len(buf)
        for i in range(0, buflen, 3):
            x = buf[i] + buf[i + 1] * 45
            if buflen - i >= 3:
                x = buf[i] + buf[i + 1] * 45 + buf[i + 2] * 45 * 45
                res.extend(list(divmod(x, 256)))
            else:
                res.append(x)
        return bytes(res)
    except (ValueError, IndexError, AttributeError):
        # raise ValueError("Invalid base45 string")
        return None


def read_coronapas(cin, print_data=False):
    # Trim first few characters of the string that are not part of the encrypted data
    # our way, just look for the first ":" char and slice the string
    for idx, char in enumerate(cin):
        if char == ':' and idx < 5:
            cin = cin[idx + 1:]
            break

    # Decode message using Base45 encoding format
    # https://datatracker.ietf.org/doc/html/draft-faltstrom-base45-02
    cin = b45decode(cin)
    if cin is None:
        return None

    # Decompress Zlib
    if (cin[0] == 0x78):
        cin = zlib.decompress(cin)

    # Decode Cose message
    decoded = CoseMessage.decode(cin)
    payload = decoded.payload

    # unpack the CBOR into JSON
    payload = cbor2.loads(payload)
    # print entries
    if print_data is True:
        claim_names = {1: "Issuer", 6: "Issued At", 4: "Expiration time", -260: "Health claims"}
        for k in payload:
            if k != -260:
                n = f'Claim {k}'
                if k in claim_names:
                    n = claim_names[k]
                print(f'{n:20}: {payload[k]}')

    # rename dict keys for convenience
    payload['issuer'] = payload.pop(1)
    payload['valid_from'] = payload.pop(6)
    payload['valid_to'] = payload.pop(4)
    # payload['token'] = payload.pop(7)
    return payload


def epoch_to_datetime(epoch_time):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch_time))


def simple_validator(qr_string, EXPIRE_OFFSET=0):
    current_time = time.time()
    drift_error = ''
    extra_time_error = ''
    status = 0   # 'coronapas_valid' in CoronapasScanner.count_scan()
    # current------valid_from------valid_to
    if current_time < qr_string['valid_from']:
        drift_error = 'POTENTIAL DRIFT ERROR BY {0:1.2f} seconds!'.format(qr_string['valid_from'] - current_time)
        status = 3  # 'coronapas_would_be_expired_scan_drift' in CoronapasScanner.count_scan()
    # valid_to------current-----valid_to+1h
    if current_time >= qr_string['valid_to'] and current_time <= qr_string['valid_to'] + EXPIRE_OFFSET:
        extra_time_error = 'WOULD BE EXPIRED WITHOUT EXTRA TIME'
        status = 4  # 'coronapas_would_be_expired_scan_extra_time' in CoronapasScanner.count_scan()
    if current_time <= qr_string['valid_to'] + EXPIRE_OFFSET:
        result = 'Simple Validator: VALID \nValid from: \t{0}\nCurrent time: \t{1} {2}\nValid to: \t{3}\nThreshold: \t{4} {5}'.format(
            epoch_to_datetime(qr_string['valid_from']),
            epoch_to_datetime(current_time),
            drift_error,
            epoch_to_datetime(qr_string['valid_to']),
            epoch_to_datetime(qr_string['valid_to'] + EXPIRE_OFFSET),
            extra_time_error)
        DEBUG(result)
        LOG.warning(result)
        return True, status
    else:
        status = 2
        result = 'Simple Validator: EXPIRED \nValid from: \t{0}\nCurrent time: \t{1} {2}\nValid to: \t{3}\nThreshold: \t{4} {5}'.format(
            epoch_to_datetime(qr_string['valid_from']),
            epoch_to_datetime(current_time),
            drift_error,
            epoch_to_datetime(qr_string['valid_to']),
            epoch_to_datetime(qr_string['valid_to'] + EXPIRE_OFFSET),
            extra_time_error)
        DEBUG(result)
        LOG.warning(result)
        return False, status


# Code below runs only if this file is executed directly. Used as module if imported.
if __name__ == '__main__':
    try:
        CAM = IMX219()
        send_app_request("coronapass_begin")
        counter = 0
        dark_counter = 0
        light_counter = 0
        dark = False
        light = True
        apply_contrast = True
        while True:
            t1 = time.time()
            apply_contrast = True
            src_img = CAM.getframe()
            enhance_status = 'g'
            src_img = src_img[50:670, 200:1080]  # Cropping. Must know src image resolution! (current is for 720x1280)
            src_img = np.float32(src_img[:, :, 0])  # Extracting grey channel (0th) from 'YUV' format. Y - greyscale
            if (counter == 5):
                counter = 0
                apply_contrast = False
            if apply_contrast == True and dark == True:
                alpha1 = -0.301
                beta1 = 26
                alpha = -0.15
                beta = -15
                variables1=[alpha1, beta1]
                variables2=[alpha, beta]
                alpha,beta = random.choice([variables1, variables2])
                src_img = cv2.convertScaleAbs(src_img, alpha=alpha, beta=beta)  # Changing contract and brightness
                ret, src_img = cv2.threshold(src_img,50,255,cv2.THRESH_BINARY)
            elif apply_contrast == True and light == True:
                alpha1 = -0.8
                beta1 = 100
                alpha = -0.8
                beta = 100
                variables1=[alpha1, beta1]
                variables2=[alpha, beta]
                alpha,beta = random.choice([variables1, variables2])
                src_img = cv2.convertScaleAbs(src_img, alpha=alpha, beta=beta)  # Changing contract and brightness
                ret, src_img = cv2.threshold(src_img,100,255,cv2.THRESH_BINARY)
            cv2.imshow("img", src_img)
            counter += 1
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
            qrcode = qr_decode_img(src_img, show_img=False)
            print('loop without timers')
            if apply_contrast == False:
                print('loop WITH timers')
                if time.time() - t1 > 0.25:
                    dark_counter += 1
                if dark_counter == 30:
                    dark = True
                    light = False
                    print("dark")
                if time.time() - t1 < 0.25:
                    dark_counter = 0
                if time.time() - t1 < 0.25:
                    light_counter += 1
                    print(light_counter)
                if light_counter == 30:
                    light = True
                    dark = False
                    print("light")
                if time.time() - t1 > 0.25:
                    light_counter = 0



            if qrcode:
                qr_string, qr_type, [x, y], qr_size_ratio_to_img = qrcode[0]
                print(qr_size_ratio_to_img)
                result = read_coronapas(qr_string)
                status = simple_validator(result)
                send_app_request('coronapass_valid')
                time.sleep(1)
                send_app_request("detection_idle")
                time.sleep(1)
                send_app_request('coronapass_begin')
    except KeyboardInterrupt:
        quit()




