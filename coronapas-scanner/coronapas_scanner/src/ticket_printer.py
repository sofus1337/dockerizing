import os
from time import time
from datetime import datetime
import usb
import copy

from PIL import Image, ImageDraw, ImageFont, UnidentifiedImageError
from brother_ql.raster import BrotherQLRaster
from brother_ql.backends import backend_factory
from brother_ql import create_label
from utils import DebugPrint, CONFIG_SETTINGS
from logger import LOG

SETTINGS = CONFIG_SETTINGS['label_printer']

DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)

DEBUG_SHOW_GENERATED_IMG = True


class LabelPrinter(object):
    CHOSEN_LANGUAGE = SETTINGS['CHOSEN_LANGUAGE']
    DEVICE_NAME = SETTINGS['DEVICE_NAME']
    PRINTER_MODEL = SETTINGS['PRINTER_MODEL']
    LABEL_SIZE = SETTINGS['LABEL_SIZE']
    BACKEND = SETTINGS['BACKEND']
    TICKET_LENGTH = SETTINGS.getint('TICKET_LENGTH')
    TICKET_WIDTH = SETTINGS.getint('TICKET_WIDTH')

    PRINT_TIMEOUT = SETTINGS.getint('PRINT_TIMEOUT')

    MAX_IMG_LENGTH_IN_TICKET = SETTINGS.getint('MAX_IMG_LENGTH_IN_TICKET')
    MAX_IMG_WIDTH_IN_TICKET = SETTINGS.getint('MAX_IMG_WIDTH_IN_TICKET')

    backend_factory = backend_factory(BACKEND)

    def __init__(self):
        if self.CHOSEN_LANGUAGE == 'dk':
            self.contents = self.get_contents_danish()
        elif self.CHOSEN_LANGUAGE == 'en':
            self.contents = self.get_contents_english()
        else:
            self.contents = self.get_contents_danish()

        self.check_usb_connection()

    def check_usb_connection(self):
        list_available_devices = self.backend_factory['list_available_devices']()
        # assert (len(list_available_devices) > 0), 'No available Brother Label printer. Check if printer is turned on.'
        if len(list_available_devices) < 1:
            DEBUG('Brother Label printer is not connected or is not turned on.')
            LOG.warning('Brother Label printer is not connected or is not turned on.')
            return False
        else:
            return True

    def print_ticket(self, corona_status, valid_to_date, qr_img=None):
        enabled = self.check_usb_connection()
        if enabled is False:
            return False

        ticket = self.create_ticket(valid_to_date, corona_status, qr_img=qr_img)
        status = self.print_brother_label(ticket)
        if status is None:
            return False
        return True

    def create_ticket(self, valid_to_coronapas, coronapas_status, qr_img=None):
        # Create empty white background image with 696x300 resolution. # "1" -> 1bit BW mode. color 1 -> white
        background = Image.new(mode='1',
                               size=(self.TICKET_LENGTH, self.TICKET_WIDTH),
                               color=1)

        if coronapas_status is True:
            coronapas_text = self.contents['coronapas_good']
        else:
            coronapas_text = self.contents['coronapas_bad']

        underqr = self.contents['coronapas_underqr']
        headline1 = self.contents['headline1']
        headline2 = self.contents['headline2']
        coronapas = self.contents['coronapas']
        valid_to = copy.deepcopy(self.contents['valid_to'])
        valid_to['text'] += str(datetime.fromtimestamp(valid_to_coronapas).strftime("%H:%M  %d.%m.%Y"))

        all_strings = [headline1, headline2, coronapas, valid_to, coronapas_text]
        # Create drawing handle on background image
        draw = ImageDraw.Draw(background)

        # Fill background with objects at specificed (x,y) pixel locations.
        for x in all_strings:
            draw.text(x['pos'], x['text'], font=ImageFont.truetype(x['font'], x['size']), fill='black')

        if qr_img is not None:
            background.paste(qr_img, self.contents['qrcode_img']['pos'])
            draw.text(underqr['pos'], underqr['text'], font=ImageFont.truetype(underqr['font'],
                      underqr['size']), fill='black')
        else:
            try:
                base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
                final_img_path = os.path.join(base_path, self.contents['qrcode_img']['rel_path'])
                qrcode_img = self.open_image(final_img_path)
                background.paste(qrcode_img, self.contents['qrcode_img']['pos'])
            except FileNotFoundError as e:
                DEBUG(e)
                LOG.critical(e)

        ready_ticket = background  # Just for clarify purposes. at this line template is filled with data.
        if DEBUG_SHOW_GENERATED_IMG is True:
            ready_ticket.show()
        return ready_ticket

    def print_brother_label(self, img):
        qlr = BrotherQLRaster(self.PRINTER_MODEL)
        qlr.exception_on_warning = True
        create_label(qlr, img, self.LABEL_SIZE)  # resizes image?
        try:
            BrotherQLBackend = self.backend_factory['backend_class']
            printer = BrotherQLBackend(self.DEVICE_NAME)
            printer.write_timeout = self.PRINT_TIMEOUT
        except (TypeError, ValueError, usb.core.USBError) as err:
            DEBUG(err)
            DEBUG("<- Brother Printer has been disconnected/disabled during the printing process.")
            LOG.warning("<- Brother Printer has been disconnected/disabled during the printing process.")
            LOG.warning(err)
            return None
        t1 = time()
        # usb.core.USBError <---- crashed printer.write with this error

        try:
            printer.write(qlr.data, )
            r = ">"
            while r:
                r = printer.read()
        except (usb.core.USBError, TypeError) as err:
            DEBUG(err)
            DEBUG("<- Brother Printer has been disconnected/disabled during the printing process.")
            LOG.warning("<- Brother Printer has been disconnected/disabled during the printing process.")
            LOG.warning(err)
            return None
        t2 = time()
        # if printing took more time than "write timeout" -> its not working. We take 90% of write_timeout for safety..
        if t2 - t1 >= (self.PRINT_TIMEOUT / 1000) - ((self.PRINT_TIMEOUT / 1000) * 0.1):
            err = 'Brother Label printer cannot print. Check if lid is closed, roll is mounted in and pushed to feeder.'
            DEBUG(err)
            LOG.warning(err)
        return True

    def open_image(self, rel_path):
        base_path = os.path.dirname(__file__)
        full_path = os.path.join(base_path, rel_path)

        if os.path.isfile(full_path):
            if '.jpg' in full_path or '.jpeg' in full_path or '.png' in full_path:
                try:
                    img = Image.open(full_path)
                    return img
                except (FileNotFoundError, UnidentifiedImageError) as e:
                    DEBUG(e)
                    LOG.critical(e)
        for file_name in sorted(os.listdir(full_path)):
            ext = os.path.splitext(file_name)[1]
            if ext == '.jpg' or ext == '.jpeg' or ext == '.png':
                img_path = os.path.join(full_path, file_name)
                try:
                    img = Image.open(img_path)
                    len, wid = img.size
                    if len > self.MAX_IMG_LENGTH_IN_TICKET:
                        new_len = self.MAX_IMG_LENGTH_IN_TICKET
                        len_diff_ratio = (len - self.MAX_IMG_LENGTH_IN_TICKET) / len
                        new_wid = wid - (wid * (len_diff_ratio))
                        if new_wid > self.MAX_IMG_WIDTH_IN_TICKET:
                            new_wid = self.MAX_IMG_WIDTH_IN_TICKET
                        new_size = (int(new_len), int(new_wid))
                        img = img.resize(new_size, Image.NEAREST)
                    return img
                except UnidentifiedImageError as e:
                    print(e)
                    break
        return None

    def get_contents_danish(self):
        danish_ticket = {
            # QR CODE IMAGE
            'qrcode_img': {'rel_path': 'misc/printer/ReallyARobot.jpg', 'pos': (500, 100)},
            # STATIC TEXT
            'underqr': {'text': 'Følg os', 'pos': (505, 257), 'font': 'Ubuntu-RI.ttf', 'size': 30},
            'coronapas_underqr': {'text': 'Din kode', 'pos': (505, 257), 'font': 'Ubuntu-RI.ttf', 'size': 30},
            'headline1': {'text': 'Tak for scan med', 'pos': (20, 20), 'font': 'Ubuntu-R.ttf', 'size': 38},
            'headline2': {'text': 'Coronapas Scanner!', 'pos': (20, 55), 'font': 'Ubuntu-R.ttf', 'size': 38},
            'coronapas': {'text': 'Billet: ', 'pos': (20, 130), 'font': 'Ubuntu-R.ttf', 'size': 30},
            'valid_to': {'text': 'Gyldig til: ', 'pos': (20, 260), 'font': 'Ubuntu-R.ttf', 'size': 25},
            # RESULT TEXT
            'coronapas_good': {'text': 'GYLDIG', 'pos': (200, 130), 'font': 'Ubuntu-B.ttf', 'size': 30},
            'coronapas_bad': {'text': 'IKKE GYLDIG', 'pos': (200, 130), 'font': 'Ubuntu-B.ttf', 'size': 30},
        }
        return danish_ticket

    def get_contents_english(self):
        english_ticket = {
            # QR CODE IMAGE
            'qrcode_img': {'rel_path': 'misc/printer/ReallyARobot.jpg', 'pos': (500, 100)},
            # STATIC TEXT
            'underqr': {'text': 'Follow us', 'pos': (490, 257), 'font': 'Ubuntu-RI.ttf', 'size': 30},
            'coronapas_underqr': {'text': 'Your code', 'pos': (505, 257), 'font': 'Ubuntu-RI.ttf', 'size': 30},
            'headline1': {'text': 'Thanks for scan with', 'pos': (20, 20), 'font': 'Ubuntu-R.ttf', 'size': 38},
            'headline2': {'text': 'Coronapas Scanner!', 'pos': (20, 55), 'font': 'Ubuntu-R.ttf', 'size': 38},
            'coronapas': {'text': 'Ticket: ', 'pos': (20, 130), 'font': 'Ubuntu-R.ttf', 'size': 30},
            'valid_to': {'text': 'Valid until: ', 'pos': (20, 260), 'font': 'Ubuntu-R.ttf', 'size': 25},
            # ==========================================================================================================
            # NEW STATIC TEXT FOR TECHNOMANIA (TODO HACK FIX PROPERLY LATER)
            'logo_img': {'rel_path': 'labels/ReallyARobot.jpg', 'pos': (20, 30)},
            'line_img': {'rel_path': 'labels/line.jpg', 'pos': (0, 250)},
            'lower_headline1': {'text': 'Coronapas Scanner', 'pos': (20, 260), 'font': 'Ubuntu-B.ttf', 'size': 28},
            'lower_headline2': {'text': 'Innovative approach to health protection',
                                'pos': (20, 295), 'font': 'Ubuntu-R.ttf', 'size': 20},
            'lower_contact1': {'text': 'Get in touch:', 'pos': (460, 260), 'font': 'Ubuntu-B.ttf', 'size': 28},
            'lower_contact2': {'text': 'www.reallyarobot.com', 'pos': (460, 295), 'font': 'Ubuntu-R.ttf', 'size': 20},
            'coronapas_good': {'text': 'VALID', 'pos': (200, 130), 'font': 'Ubuntu-B.ttf', 'size': 25},
            'coronapas_bad': {'text': 'NOT VALID', 'pos': (200, 130), 'font': 'Ubuntu-B.ttf', 'size': 25},
            # ==========================================================================================================
        }
        return english_ticket

# export BROTHER_QL_PRINTER=usb://0x04f9:0x2042
# export BROTHER_QL_MODEL=QL-700


if __name__ == '__main__':
    try:
        LP = LabelPrinter()
        LP.print_ticket(True, time())
    except KeyboardInterrupt:
        quit()
