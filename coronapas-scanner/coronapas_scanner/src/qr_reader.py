import time
import os
import cv2
from pyzbar import pyzbar
import qrcode
import numpy as np
import random

from utils import DebugPrint, CONFIG_SETTINGS

SETTINGS = CONFIG_SETTINGS['common']
IMX_OR_REALSENSE = SETTINGS.getint('IMX_OR_REALSENSE')
# Only of the two can be used, but still importing both
if IMX_OR_REALSENSE == 0:  # IMX:
    from imx219 import IMX219
elif IMX_OR_REALSENSE == 1:  # RealSense
    from realsense import RealSense


DEBUG_MODE = True
DEBUG = DebugPrint(DEBUG_MODE)


def make_qrcode(qr_string):
    return qrcode.make(qr_string)


def qr_decode_video_RealSense():
    RS = RealSense()
    while True:
        # RS_scale, RS_intrin = RS.getDepthParams()
        img, _ = RS.getframe()
        barcodes = pyzbar.decode(img)
        for barcode in barcodes:
            (x, y, w, h) = barcode.rect
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type
            return barcodeData, barcodeType


def qr_decode_video_IMX219(show_img=False):
    CAM = IMX219()
    while True:
        img = CAM.getframe()
        barcodes = pyzbar.decode(img)
        for barcode in barcodes:
            (x, y, w, h) = barcode.rect
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type
            if show_img is True:
                text = "{} ({})".format(barcodeData, barcodeType)
                cv2.putText(img, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                cv2.imshow("image width: {0}, height: {1}.".format(img.shape[1], img.shape[0]), img)
                cv2.waitKey()
                cv2.destroyAllWindows()
            return barcodeData, barcodeType


def qr_decode_img(img, save_img=False, show_img=False, enhance_comment=None):
    barcodes = pyzbar.decode(img)
    results = []
    if len(img.shape) == 3:
        [img_width, img_height, _] = img.shape  # original image has [width,height,channels] format
    else:
        [img_width, img_height] = img.shape  # 'enhanced' image has [width,height] format

    img_pixel_area = img_width * img_height
    for barcode in barcodes:
        (x, y, w, h) = barcode.rect
        barcode_pixel_area = w * h
        qrcode_to_img_ratio = barcode_pixel_area / img_pixel_area
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
        barcodeData = barcode.data.decode("utf-8")
        barcodeType = barcode.type
        text = "{} ({})".format(barcodeData, barcodeType)
        if show_img is True:
            cv2.putText(img, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.imshow("img", img)
            cv2.waitKey()
            cv2.destroyAllWindows()
        if save_img and qrcode_to_img_ratio > 0.02:
            base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            img_rel_path = 'log/images/'
            date_time = time.strftime("%Y.%m.%d_%H:%M:%S", time.localtime())
            if enhance_comment is None:
                name = "{0}_type-{1}_size-{2:1.2f}.jpg".format(date_time, barcodeType, qrcode_to_img_ratio)
            elif type(enhance_comment) == str:
                name = "{0}_type-{1}_size-{2:1.2f}_{3}.jpg".format(
                    date_time, barcodeType, qrcode_to_img_ratio, enhance_comment)
            full_img_path = os.path.join(base_path, img_rel_path, name)
            cv2.imwrite(full_img_path, img)
        DEBUG("Found {} barcode: {}".format(barcodeType, barcodeData))
        results.append([barcodeData, barcodeType, [x, y], qrcode_to_img_ratio])
    return results


# =================== IMAGE POST-PROCESSING FOR BETTER QR-CODE READING ===================
# ========================================================================================


def RS_enhance_image(src_img):
    enhance_status = 'g'
    src_img = src_img[30:420, 240:640]  # Must know src image resolution! (current is for 480x640)
    src_img = rgb2gray(src_img)  # Gray scaling
    #src_img = unsharp_mask(src_img)  # Sharpening
    #src_img = remove_background(src_img)  # Substracting background
    apply_contrast = random.choice([True, False])
    if apply_contrast is True:
        choices = [[-0.65, 90], [-0.8, 100]]
        [alpha, beta] = random.choice(choices)
        for idx, pair in enumerate(choices):
            if [alpha, beta] == pair:
                if idx == 0:
                    enhance_status = 'gcl'
                elif idx == 1:
                    enhance_status = 'gch'
        src_img = cv2.convertScaleAbs(src_img, alpha=alpha, beta=beta)  # Changing contract and brightness
    return src_img, enhance_status


def IMX_enhance_image(src_img):
    enhance_status = 'g'
    src_img = src_img[70:650, 400:1280]  # Cropping. Must know src image resolution! (current is for 720x1280)
    src_img = np.float32(src_img[:, :, 0])  # Extracting grey channel (0th) from 'YUV' format. Y - greyscale
    apply_contrast = random.choice([True, False])
    if apply_contrast is True:
        choices = [[-0.65, 90], [-0.8, 100]]
        [alpha, beta] = random.choice(choices)
        for idx, pair in enumerate(choices):
            if [alpha, beta] == pair:
                if idx == 0:
                    enhance_status = 'gcl'
                elif idx == 1:
                    enhance_status = 'gch'
        src_img = cv2.convertScaleAbs(src_img, alpha=alpha, beta=beta)  # Changing contract and brightness
    return src_img, enhance_status


# Realsense way. Not for IMX camera
def rgb2gray(rgb_img):
    return np.dot(rgb_img[..., :3], [0.2989, 0.5870, 0.1140])


def unsharp_mask(image, kernel_size=(5, 5), sigma=1.2, amount=1.2, threshold=0):
    """Return a sharpened version of the image, using an unsharp mask."""
    blurred = cv2.GaussianBlur(image, kernel_size, sigma)
    sharpened = float(amount + 1) * image - float(amount) * blurred
    sharpened = np.maximum(sharpened, np.zeros(sharpened.shape))
    sharpened = np.minimum(sharpened, 255 * np.ones(sharpened.shape))
    sharpened = sharpened.round().astype(np.uint8)
    if threshold > 0:
        low_contrast_mask = np.absolute(image - blurred) < threshold
        np.copyto(sharpened, image, where=low_contrast_mask)
    return sharpened


def remove_background(img):
    # Morph-dilate-op
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    dilated = cv2.morphologyEx(img, cv2.MORPH_DILATE, kernel)

    # MedianBlur
    median = cv2.medianBlur(dilated, 15)
    diff2 = 255 - cv2.subtract(median, img)

    # Normalize
    normed = cv2.normalize(diff2, None, 0, 255, cv2.NORM_MINMAX)
    return normed


if __name__ == '__main__':
    try:
        # qr_decode_video(video_input=4)
        path = '/home/eugenegalaxy/Documents/1.jpg'
        img = cv2.imread(path)
        qr_decode_img(img, save_img=True)
    except KeyboardInterrupt:
        quit()
